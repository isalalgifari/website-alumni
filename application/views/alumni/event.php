<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

<div class="row">
        
        <div class="col-lg">

            <?= $this->session->flashdata('message'); ?>
            <table class="table table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Event Name</th>
                    <th scope="col">Theme</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($event as $a) : ?>
                    <tr>
                    <th scope="row"><?= $i; ?></th>
                    <td><?= $a['name']; ?></td>
                    <td><?= $a['theme']; ?></td>
                    <td>
                    <a href="" data-toggle="modal" data-target=".bd-example-modal-lg<?=$a['id']; ?> " class="badge badge-success badge-sm"><i class="fas fa-info-circle"></i></a>
                    </td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!--modal-->
<?php $no=0; foreach($event as $a): $no++; ?>
    <div class="modal fade bd-example-modal-lg<?=$a['id']; ?>" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModalLabel">School Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('alumni'); ?>" method="post">
      <input type="hidden" name="id" id="id" value="<?=$a['id']; ?>">
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <img src="<?= base_url('assets/img/event/') . $a['image']; ?>" class="img-thumbnail">
                        </div>    
                    </div>
                    <div class="col-4 col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Event Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?=$a['name']; ?>" placeholder="... event name ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Theme</label>
                            <input type="text" class="form-control" id="theme" name="theme" value="<?=$a['theme']; ?>" placeholder="... theme ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Location</label>
                            <input type="text" class="form-control" id="location" name="location" value="<?=$a['location']; ?>" placeholder="... location ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date</label>
                            <input type="text" class="form-control" id="date" name="date" value="<?=$a['date']; ?>" placeholder="... date ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Information</label>
                            <textarea type="text" class="form-control" id="information" name="information" placeholder="... information ..."><?=$a['information']; ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach; ?>


