<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

<div class="row">
        
        <div class="col-lg">

            <h2 class="text-center mb-5">DATA ALUMNI </h2>
            <?= $this->session->flashdata('message'); ?>
            <a href="<?= base_url('admin/index'); ?>" class="btn btn-primary mb-3">back</a>
            <table class="table table-hover">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Period</th>
                    <th scope="col">University</th>
                    <th scope="col">Major</th>
                    <th scope="col">Company</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach($alumni as $a) : ?>
                    <tr>
                    <th scope="row"><?= $i; ?></th>
                    <td><?= $a['name']; ?></td>
                    <td><?= $a['period']; ?></td>
                    <td><?= $a['school']; ?></td>
                    <td><?= $a['major']; ?></td>
                    <td><?= $a['company']; ?></td>
                    <td>
                    <a href="" data-toggle="modal" data-target=".bd-example-modal-lg<?=$a['id']; ?> " class="badge badge-success badge-sm"><i class="fas fa-info-circle"></i></a>
                    <a href="<?= base_url('admin/editUser/') . $a['id']; ?>" class="badge badge-info badge-sm"><i class="fas fa-pencil-alt"></i></a>
                    <a href="<?= base_url('admin/deleteUser/') . $a['id']; ?>" class="badge badge-danger badge-sm" onclick="return confirm('are you sure ?')"><i class="far fa-times-circle"></i></a>
                    </td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!--modal-->
<?php $no=0; foreach($alumni as $a): $no++; ?>
    <div class="modal fade bd-example-modal-lg<?=$a['id']; ?>" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="formModalLabel">Profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('alumni'); ?>" method="post">
      <input type="hidden" name="id" id="id" value="<?=$a['id']; ?>">
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <img src="<?= base_url('assets/img/profile/') . $a['image']; ?>" class="img-thumbnail">
                        </div>    
                        <div class="form-group">
                            <label for="exampleInputEmail1">Role : " <?=$a['role_id']; ?> "</label>
                        </div>
                        <div class="form-group">
                            <small class="text-muted">Member since <?= date('d F Y', $a['date_created']); ?></small>
                        </div>
                    </div>
                    <div class="col-4 col-sm-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?=$a['name']; ?>" placeholder="... name ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?=$a['email']; ?>" placeholder="... email ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">OSIS Period</label>
                            <input type="text" class="form-control" id="period" name="period" value="<?=$a['period']; ?>" placeholder="... period ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">University or Institute</label>
                            <input type="text" class="form-control" id="school" name="school" value="<?=$a['school']; ?>" placeholder="... school ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Major</label>
                            <input type="text" class="form-control" id="major" name="major" value="<?=$a['major']; ?>" placeholder="... major ...">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Company</label>
                            <input type="text" class="form-control" id="company" name="company" value="<?=$a['company']; ?>" placeholder="... company ...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php endforeach; ?>


