<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title ?></h1>

<div class="container">
    <input type="hidden" name="id" value="<?= $alumni['id']; ?>">
    <form action="" method="post">
        <div class="row mt-3">
            <div class="col-md-4">
                <div class="form-group">
                    <img src="<?= base_url('assets/img/profile/') . $alumni['image']; ?>" class="img-thumbnail">
                </div>
                <div class="custom-file mt-2">
                    <input type="file" class="custom-file-input" id="image" name="image">
                    <label class="custom-file-label" for="image">Choose File</label>
                </div>
                <div class="form-group mt-3">
                    <button type="submit"  class="btn btn-success float-right">edit</button>
                    <a href="<?= base_url('admin'); ?>" class="btn btn-primary float-right ml-1 mr-1">back</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="text">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="<?= $alumni['email'];?>" readonly>
                    <small  class="form-text text-danger"><?= form_error('email'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?= $alumni['name'];?>">
                    <small  class="form-text text-danger"><?= form_error('name'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">OSIS Period</label>
                    <input type="text" class="form-control" id="period" name="period" value="<?= $alumni['period'];?>">
                    <small  class="form-text text-danger"><?= form_error('period'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">University</label>
                    <input type="text" class="form-control" id="school" name="school" value="<?= $alumni['school'];?>">
                    <small  class="form-text text-danger"><?= form_error('school'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Major</label>
                    <input type="text" class="form-control" id="major" name="major" value="<?= $alumni['major'];?>">
                    <small  class="form-text text-danger"><?= form_error('major'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Company</label>
                    <input type="text" class="form-control" id="company" name="company" value="<?= $alumni['company'];?>">
                    <small  class="form-text text-danger"><?= form_error('company'); ?></small>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="text">Role ID <br> 1 = ADMIN  |  2 = USER</label>
                    <input type="text" class="form-control" id="role_id" name="role_id" value="<?= $alumni['role_id'];?>">
                    <small  class="form-text text-danger"><?= form_error('role_id'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Active <br> 1 = Active  |  2 = Not Active</label>
                    <input type="text" class="form-control" id="is_active" name="is_active" value="<?= $alumni['is_active'];?>">
                    <small  class="form-text text-danger"><?= form_error('is_active'); ?></small>
                </div>
                <div class="form-group">
                    <label for="text">Password</label>
                    <input type="password" class="form-control" id="new_password" name="new_password">
                    <small  class="form-text text-danger"><?= form_error('new_password'); ?></small>
                </div>
            </div>
        </div>
    </form>
</div>


</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->