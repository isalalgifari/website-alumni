<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
    <div class="col-sm-6">
        <div class="card">
        <div class="card-body">
            <h5 class="card-title text-center">Data Alumni</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="<?= base_url('admin/data'); ?>" class="btn btn-primary">Go somewhere</a>
        </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
        <div class="card-body">
            <h5 class="card-title text-center">Event's</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="<?= base_url('admin/event'); ?>" class="btn btn-primary">Go somewhere</a>
        </div>
        </div>
    </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

