<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Menu_model');
        $this->load->model('Auth_model');

        check_login();
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////MENU

    public function index()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        //ambil data user menu
        //menggunakan result array krn ingi mengambil banyak data
        //row array untuk mengambil satu baris data saja
        $data['menu'] = $this->db->get('user_menu')->result_array();
        
        $this->form_validation->set_rules('menu', 'Menu', 'required');
        
        if( $this->form_validation->run() == false ){
            $data['title'] = 'Menu Management';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/index', $data);
            $this->load->view('templates/footer');
        } else{
            $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New menu added!
            </div>');
              redirect('menu');
        }
    }

    public function deletemenu($id)
    {
        $this->Menu_model->deleteMenu($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Menu success delete !
            </div>');
              redirect('menu');
    }

   


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////SUBMENU

    public function submenu()
    {
        $data['user'] = $this->Auth_model->email();

        $data['subMenu'] = $this->Menu_model->getSubMenu()->result_array();

        $data['menu'] = $this->db->get('user_menu')->result_array();


        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('url', 'Url', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if( $this->form_validation->run() == false ){
            $data['title'] = 'Submenu Management';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/submenu', $data);
            $this->load->view('templates/footer');
        }else{
            $this->Menu_model->submenuadd();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New submenu added!
            </div>');
              redirect('menu/submenu');
        }
       
    }

    public function editSubMenu($id)
    {
        $data['user'] = $this->Auth_model->email();
        
        $data['sub'] = $this->Menu_model->getSubMenuById($id);

        $this->form_validation->set_rules('menu_id', 'Menu', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('url', 'Url', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');
        $this->form_validation->set_rules('is_active', 'is_active', 'required');

        if( $this->form_validation->run() == false ){
            $data['title'] = 'Edit Submenu';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('menu/editsub', $data);
            $this->load->view('templates/footer');
        }else{
            $this->Menu_model->ubahDataSubMenu();
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Success edit submenu!
            </div>');
              redirect('menu/submenu');
        }
    }

    public function deletesubmenu($id)
    {
        $this->Menu_model->deleteSubMenu($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Submenu success delete !
            </div>');
              redirect('menu/submenu');
    }

}