<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('Auth_model');
        $this->load->model('Admin_model');

        check_login();
    }

    public function index()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        $data['alumni'] = $this->db->get('user')->result_array();
        
        $data['title'] = 'Admin Page';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }

    
    ////////////////////////////////////////////////////////////
    //////// DATA ALUMNI ////////////////////////////////

    public function data()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        $data['alumni'] = $this->db->get('user')->result_array();
        
        $data['title'] = 'Admin Page';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/dataalumni', $data);
        $this->load->view('templates/footer');
    }

    public function deleteUser($id)
    {
        $this->Admin_model->deleteuser($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">User success deleted ! </div>');
        redirect('admin/data');
    }

    public function editUser($id)
    {
        $data['user'] = $this->Auth_model->email();

        $data['alumni'] = $this->Admin_model->getUserById($id);

        $this->form_validation->set_rules('new_password', 'New Password', 'min_length[5]');
        
        if( $this->form_validation->run() == false ){
            $data['title'] = 'Admin Page';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/editalumni', $data);
            $this->load->view('templates/footer');
        }else{

            $name =$this->input->post('name');
            $email =$this->input->post('email');
            $new_password = $this->input->post('new_password');
            $role_id =$this->input->post('role_id');
            $is_active =$this->input->post('is_active');
            $school =$this->input->post('school');
            $major =$this->input->post('major');
            $period =$this->input->post('period');
            $company =$this->input->post('company');
            $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

            //cek jika ada gambar yg akan di upload
            $upload_image = $_FILES['image']['name'];

            if( $upload_image ){
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile';
                $this->load->library('upload', $config);

                if( $this->upload->do_upload('image') ) {
                    $old_image = $data['user']['image'];
                    if( $old_image != 'default.jpg' ){
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                }else {
                    echo $this->upload->display_errors();
                }
            }
           
            $this->db->set('name', $name);
            $this->db->set('email', $email);
            $this->db->set('password', $password_hash);
            $this->db->set('role_id', $role_id);
            $this->db->set('is_active', $is_active);
            $this->db->set('school', $school);
            $this->db->set('major', $major);
            $this->db->set('period', $period);
            $this->db->set('company', $company);
            $this->db->where('id', $id);
            $this->db->update('user');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Profile has been updated </div>');

            redirect('admin/data');

        }
       
    }




/////////////////////////////////////////////////////////////////
////////////////////////////    EVENT    //////////////////////////////////////

    public function event()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        $data['event'] = $this->db->get('event')->result_array();
        
        $this->form_validation->set_rules('name', 'Event Name', 'required');

        if( $this->form_validation->run() == false ){
            $data['title'] = 'Admin Page';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/event', $data);
            $this->load->view('templates/footer');
        } else{

        $this->Admin_model->tambahEvent();
        
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New Event added!
            </div>');
            redirect('admin/event');
        }
    }

    public function deleteevent($id)
    {
        $this->Admin_model->deleteevent($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Event success deleted ! </div>');
        redirect('admin/event');
    }

    public function editevent($id)
    {
        $data['user'] = $this->Auth_model->email();

        $data['event'] = $this->Admin_model->getEventById($id);

        $this->form_validation->set_rules('name', 'Event Name', 'required');
        
        if( $this->form_validation->run() == false ){
            $data['title'] = 'Admin Page';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/editevent', $data);
            $this->load->view('templates/footer');
        }else{

            $name =htmlspecialchars($this->input->post('name', true));
            $location =htmlspecialchars($this->input->post('location', true));
            $date =htmlspecialchars($this->input->post('date', true));
            $theme =htmlspecialchars($this->input->post('theme', true));
            $information =htmlspecialchars($this->input->post('information', true));

            //cek jika ada gambar yg akan di upload
            $upload_image = $_FILES['image']['name'];

            if( $upload_image ){
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile';
                $this->load->library('upload', $config);

                if( $this->upload->do_upload('image') ) {
                    $old_image = $data['user']['image'];
                    if( $old_image != 'default.jpg' ){
                        unlink(FCPATH . 'assets/img/event/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                }else {
                    echo $this->upload->display_errors();
                }
            }
           
            $this->db->set('name', $name);
            $this->db->set('location', $location);
            $this->db->set('date', $date);
            $this->db->set('theme', $theme);
            $this->db->set('information', $information);
            $this->db->where('id', $id);
            $this->db->update('event');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Event has been updated </div>');

            redirect('admin/event');


        }
       
    }



/////////////////////////////////////////////////////////////////
////////////////////////////    ROLE    //////////////////////////////////////



    public function role()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        $data['role'] = $this->db->get('user_role')->result_array();
        
        $data['title'] = 'Role';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }
    
    //menambahkan role
    public function addrole()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();

        $data['role'] = $this->db->get('user_role')->result_array();

        $this->form_validation->set_rules('role', 'Role', 'required');
        
        if( $this->form_validation->run() == false ){
            $data['title'] = 'Role';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('admin/role', $data);
            $this->load->view('templates/footer');
        } else{
            $this->db->insert('user_role', ['role' => $this->input->post('role')]);

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">New role added!
            </div>');
              redirect('admin/role');
        }
    }

    //delete role
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_role');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Role success delete !
            </div>');
              redirect('admin/role');
    }

    public function roleAccess($role_id)
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('user_menu')->result_array();
        
        $data['title'] = 'Role Access';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer');
    }

    public function changeaccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if( $result->num_rows() <1 ) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
         Access Changed!
        </div>');

    }

    

}