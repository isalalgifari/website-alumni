<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        check_login();

        $this->load->model('Auth_model');
    }

    public function index()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        
        $data['title'] = 'My Profile';
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }
    
    public function edit()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        
        $this->form_validation->set_rules('name', 'Full Name', 'required|trim');
        $this->form_validation->set_rules('period', 'Period', 'required|trim');

        if( $this->form_validation->run() == false ){
            $data['title'] = 'Edit Profile';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/edit', $data);
            $this->load->view('templates/footer');
        }else{
            $name = htmlspecialchars($this->input->post('name', true));
            $email = htmlspecialchars($this->input->post('email', true));
            $school = htmlspecialchars($this->input->post('school', true));
            $major = htmlspecialchars($this->input->post('major', true));
            $period = htmlspecialchars($this->input->post('period', true));
            $company = htmlspecialchars($this->input->post('company', true));
            //cek jika ada gambar yg akan di upload
            $upload_image = $_FILES['image']['name'];

            if( $upload_image ){
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2048';
                $config['upload_path'] = './assets/img/profile';
                $this->load->library('upload', $config);

                if( $this->upload->do_upload('image') ) {
                    $old_image = $data['user']['image'];
                    if( $old_image != 'default.jpg' ){
                        unlink(FCPATH . 'assets/img/profile/' . $old_image);
                    }

                    $new_image = $this->upload->data('file_name');
                    $this->db->set('image', $new_image);
                }else {
                    echo $this->upload->display_errors();
                }
            }

            $this->db->set('name', $name);
            $this->db->set('school', $school);
            $this->db->set('major', $major);
            $this->db->set('period', $period);
            $this->db->set('company', $company);
            $this->db->where('email', $email);
            $this->db->update('user');

            //memunculkan pesan berhasil registrasi
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
           Your Profile has been updated
          </div>');

            //setelah berhasil insert data arahkan ke halaman login
            redirect('user');
        }
       
    }

    public function changepassword()
    {
        //ambil data dari session berdasarkan email yang login
        //ketika melakukan login di controller auth _login
        $data['user'] = $this->Auth_model->email();
        
        $this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
        $this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[5]|matches[new_password2]');
        $this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[5]|matches[new_password1]');
    
        if( $this->form_validation->run() == false ){
            $data['title'] = 'Change Password';
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('user/changepassword', $data);
            $this->load->view('templates/footer');
        }else{

            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password1');

            if( !password_verify($current_password, $data['user']['password']) ){
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Wrong Current Password!
               </div>');
                 redirect('user/changepassword');
               }else {
    
                    //passwordnya sama yg lama dan baru
                if( $current_password == $new_password ){
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                    New password cannot be the same as current password!
                </div>');
                    redirect('user/changepassword');
                } else {

                    //change password sudah benar
                    $password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                    //update data memakai cara codeigniter
                    $this->db->set('password', $password_hash);
                    $this->db->where('email', $this->session->userdata('email'));
                    $this->db->update('user');

                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    Password changed!</div>');
                    redirect('user/changepassword');
                }
            }
        }
    }

}