<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

    public function email()
    {
       return $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    }

    public function tambahRegis(){
        
        //memakai querl dari codeigniter
        $data = [
            'name' => htmlspecialchars($this->input->post('name', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'image' => 'default.jpg',
            'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
            'role_id' => 2,
            'is_active' => 1,
            'date_created' => time(),
            'school' => '',
            'major' => '',
            'period' => htmlspecialchars($this->input->post('period', true)),
            'company' => ''
       ];

       $this->db->insert('user', $data);
    }

}