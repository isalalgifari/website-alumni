$(function() {
    
    //add
    $('.add').on('click', function(){
        $('#formModalLabel').html('Add Menu');
        //css selector
        $('.modal-footer button[type=submit]').html('Add');
    });
        

    //edit
    $('.edit').on('click', function(){
        $('#formModalLabel').html('Edit Menu');
        //css selector
        $('.modal-footer button[type=submit]').html('Update');
        $('.modal-body form').attr('action', 'http://localhost/alumni/menu/ubah');
        const id = $(this).data('id');
        //ajax edit
        $.ajax({
            url: 'http://localhost/alumni/menu/getubah',
            data: {id : id},
            method: 'post',
            dataType: 'json',
            success: function(data){
                $('#menu').val(data.menu);
                $('#id').val(data.id);
            }
        });
    });
    

});